class Calculator  {
    constructor(totalSockets, sockets, requirements, chances){
        this.requirements = requirements;
        this.sockets = sockets;
        this.totalSockets = totalSockets;
        this.chances = chances || this.getSocketChance();
        this.freeSockets = this.totalSockets - sockets.reduce((x,el)=>x+=el);
        this.voriciRecipes = this.getValidRecipes();
        //this.calculateVorici();
    }
    getSocketChance(){ 
        // https://github.com/Siveran/siveran.github.io thx for prodiving constants and algorythm
        // Constants! These are the hardest 3 numbers to find.
		const X = 5; // The X in [(Requirement + X + C) / (Requirement + 3X + C)] for on-color mono-requirement chances
		const C = 5; // The C in the above. Off-colors don't get the +C in the numerator, but do get X.
		const maxColorChance = 0.9; // What the on-color chance appears to approach with extremely high requirements.
        const requirmentAmount = this.requirements.reduce((value, req)=>
            {
                if(req!=0) return value+1;
                return value;
            }, 0);
        const totalRequirements = this.requirements.reduce((value, req)=>value + req);
        let chances;
        switch(requirmentAmount) {
            case 1:
                //monorequirement items
                chances=this.requirements.map(req=>{
                    if(req==0) return ((1-maxColorChance)/2) + maxColorChance *(X/(totalRequirements+ 3*X + C));
                     return maxColorChance * (X + C + req) / (totalRequirements + 3 * X + C);
                });
                break;
            case 2:
                chances=this.requirements.map(req=>{
                    if(req==0) return 1 - maxColorChance; //in case of double requirement items the off chance takes whole 1-maxColorChance
                    return maxColorChance * req / totalRequirements;
                })
                break;
            case 3:
                chances=this.requirements.map(req=>req/totalRequirements);
                break;
        }
        return chances;
    }
    jewellerMethod(){
        const cost = [];
        const sortedChances = this.sockets.map((el, i)=>[el, this.chances[i], i])
                    .filter(el=>el[0]!=0)
                    .sort((x,y)=>x[1]-y[1]); //By that we get  [[sockets, chance, index before filter]...] without arrays containing zeros and lowest chance pairs at the beginning
        console.log("Sorted chances:", sortedChances);
        let firstTwoSockets = [0, 0, 0];
        if(sortedChances[0][0]>=2){
            sortedChances[0][0]-=2;
            firstTwoSockets[sortedChances[0][2]]+=2;
        }else{
            sortedChances[0][0]-=1;sortedChances[1][0]-=1;
            firstTwoSockets[sortedChances[0][2]]+=1; firstTwoSockets[sortedChances[1][2]]+=1;
        }
        const jewPrices = [3, 10, 70, 350];
        //TODO ЧУТЬ ЧУТЬ ОСТАЛОСЬ ГОСПОДИ
        // sortedChances.reduce((val, el, i, arr)=>{
        //     if(el[0]!=0)
        // }, 0)
        // const twoSocketCalc = new Calculator(2, firstTwoSockets, null, this.chances);
        // let firstStep = twoSocketCalc.getFastestVorici();
        //  // eslint-disable-next-line   
        // console.log("First step:", firstStep);  
        // if(chancesWithColors[0]>=2){
        //     new Ca
        // }
    }
    factorial(num){
        let rval=1;
            for (var i = 2; i <= num; i++) rval = rval * i;
        return rval;
    }
    getValidRecipes(){
        let recipes =  [[1,0,0,4],[0,1,0,4],[0,0,1,4],[2,0,0,25],[0,2,0,25],[0,0,2,25],[3,0,0,120],[0,3,0,120],[0,0,3,120],[1,1,0,15],[1,0,1,15],[0,1,1,15], [2,1,0,100],[1,2,0,100],[0,2,1,100],[0,1,2,100],[2,0,1,100],[1,0,2,100]];
        return recipes.filter((recipe)=>{
            return recipe.slice(0,-1).every((el, i)=>this.sockets[i]>=el)
        })
        //We won't need vorici recipes like 3r, when trying to get 2b, and we won't be able to use 3r having only 1 socket

    }
    calculateVorici(){
        return this.voriciRecipes.map((recipe)=>{
            let chance;
            let sockets = this.sockets.map((el, i)=>el-recipe[i]);
            {
                let totalSockets = this.totalSockets - recipe.slice(0,-1).reduce((val, el)=>val+=el);
                // eslint-disable-next-line no-console
                console.log("Recipe/prob", recipe, this.calculateProbability(sockets, this.freeSockets, totalSockets));

                chance = this.calculateProbability(sockets, this.freeSockets, totalSockets);
            }
            const obj = {
                type: "vorici",
                cost: [
                    {
                    amount: recipe[3],
                    currency: "chromatic"
                    }
                ],
                chance: chance,
                recipe: recipe.slice(0, -1)
            }
            return obj
        })
    }
    
    calculateProbability(sockets, freeSockets, totalSockets, pos=0){
        //This method calculates probability of getting given combination of colours with given amount of free sockets
        if(freeSockets==0){
            //If there is no free sockets left, we count all possible orders (RRB,RBR,BRR for example) with this formula: ALL!/(R!*B!*G!)
            //Then we multiply chance of getting single combination (Chance of getting color^Number of desired sockets with following color for each color)
            return this.factorial(totalSockets)*sockets.reduce((val, el, i)=>val=val/this.factorial(el)*Math.pow(this.chances[i], el), 1);
        } else {

            //With that we count all possible colours on free sockets, and thanks to pos=<i we don't get 2 same but different order combinations
            return sockets.reduce((val, el, i, arr)=>{
                if(pos<=i){
                    let newSockets = arr.slice();
                    newSockets[i]+=1;
                    val+=this.calculateProbability(newSockets, freeSockets-1, totalSockets, i);
                }
                return val
            }, 0);
        }
    }
    getAllMethods(){
        let methods = [];
        methods = methods.concat(this.calculateVorici());
        return methods;
        //TODO jeweller method
        // if(this.totalSockets>2){

        // }
    }
    getFastestVorici(){
        let methods = this.calculateVorici();
        //TODO later finish sorting
        methods.sort((x, y)=>(x.cost[0].amount/x.chance)-(y.cost[0]/y.chance));
        return methods; // returns last element, the one with the highest efficiency
    }
    
    
}
export default Calculator